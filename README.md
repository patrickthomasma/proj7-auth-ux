# Project 7 auth-ux
This is my project in which I implement a display window/database unto the project 4 project, And then now I implemenet a REST API onto my database and display times in csv/json formats and now I added a login/logout/register functionality onto the webpage.
# Style
In my implementation I decided to do alot of the legwork inside of calc.html for my database and when it came to the flask implementation I just tracked through what I've already updated and added then opened a display window which would grab the information I had added onto submit and then just display how I thought would look good to display

After this I have a php folder which displays in either csv or json depending on what the user puts down
This is done by having routes in the api.py file that will get the necessary information and send it to the php file which depending on if its csv or json will display in the according format, for json I made it so it's organized as a data block while csv is just plaintext.

For my login/logout I have a menu that takes in a login and upon a succesful login then the user will see the calc.html page, user will need to type in the webpage /api/login in order to get too the login page.
# How to use
There is a script in the program called run.sh, all you have to do is type into the terminal ./run.sh and the webpage will be up! (Make sure you enable proper permissions on the script ex: chmod 777 run.sh) if the user puts in a incorrect mileage/km then the program will return the current date and time to symbolize that is an improper way of putting in the data, if user doesn't submit any data display page will be blank
For the csv/json part user will have to enter in the information they want to get from the list onto the browser (ex. 0.0.0.0:5000/listAll)
And for logging in the user will have to put the information in the browser(/api/login)
# Testing
In the Dockerfile there is a step to run the testing file and if downloaded right the tests should come out      a-okay!!!
No testing for csv/json

#NOTES:
In this current version of the project the display functions from project 6 are a bit bugged but everything required of Project 7 is working.

# User information
Written by Patrick Thomasma
Email: pthomasm@uoregon.edu


