# Laptop Service
from bson import json_util
import json
from flask_restful import Resource, Api
from flask import Flask

from flask_wtf import FlaskForm, csrf
from wtforms import StringField,PasswordField,BooleanField,validators

from passlib.apps import custom_app_context as pwd_context

from itsdangerous import (TimedJSONWebSignatureSerializer \
                                    as Serializer, BadSignature, \
                                    SignatureExpired)
from flask_login import (LoginManager, current_user, login_required,
                            login_user,logout_user,UserMixin, confirm_login,
                            fresh_login_required)

import os
import flask
from flask import request, Response,redirect,url_for,render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from datetime import datetime as datetime
import dateutil.parser
import time
from pymongo import MongoClient
import logging
logging.basicConfig(foramt='%(levelname)s:%(message)2',
                    level=logging.INFO)
log = logging.getLogger(__name__)
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = 'yeah not actually a secret'
the_csrf = csrf.CSRFProtect(app)
the_csrf.init_app(app)

client = MongoClient('mongodb://mongodb:27017/')


def hash_password(password):
    return pwd_context.encrypt(password)
def verify_password(password, hashVal):
    return pwd_context.verify(password,hashVal)

def generate_auth_token(username,expiration = 600):
    s = Serializer('test1234@#$',expires_in=expiration)
    return s.dumps({'username':username})

def verify_auth_token(token):
    s = Serializer('test1234@#$')
    try:
        data = s.loads(token)
    except SignatureExpired:
        return False
    except BadSignature:
        return False
    return True


login_manager = LoginManager()
login_manager.login_view = 'login'
login_manager.refresh_view = 'reauth'
login_manager.setup_app(app)


class User(UserMixin):
    def __init(self,name,token = "bad_token"):
        self.username = username
        self.token = token
    
    def is_active(self):
        return True
    def is_authenticated(self):
        return True
    def is_anonymous(self):
        return False
    def get_id(self):
        return self.username

db = client.calcdb
pdb = client.pdb
pdb.pdb.delete_many({})

@login_manager.user_loader
def load_user(username):
    u = pdb.pdb.find_one({"name":username})
    if not u:
        return None
    return User(u["name"])

class LoginForm(FlaskForm):
    username = StringField('username',validators=[validators.InputRequired()])
    password = PasswordField('password',validators=[validators.InputRequired()])
    remember = BooleanField('remember me')

class RegisterForm(FlaskForm):
    username = StringField('username',validators=[validators.InputRequired()])
    password = PasswordField('password',validators=[validators.InputRequired()])

class BasicForm(FlaskForm):
    pass

@app.route('/api/register',methods=['GET','POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        try:
            name = request.form['username']
            password = request.form['password']
            test_pass = hash_password(password)
            test_doc = {'name':name , 'password':test_pass,'token':"bad_token"}
            pdb.pdb.insert_one(test_doc)
            return redirect(url_for("index"))
        except:
            response = Response(status=400)
            return response
    return render_template('register.html',form = form)

@app.route('/api/token',methods=['GET','POST'])
@login_required
def token():
    if current_user.is_authenticated:
        token = generate_auth_token(current_user.username)
        u = pdb.pdb.find_one({"name": current_user.username})

        if not u:
            return None
        pdb.pdb.update({"_id":u["_id"]},{"$set":{"token":token}})
        current_user.token = token
        return flask.jsonify({'duratoin':600, 'token':token.decode('ascii')})
    else:
        return redirect(url_for('login'))

@app.route("/api/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("index"))
    form = LoginForm()
    if form.validate_on_submit():
        user = pdb.pdb.find_one({"name": request.form["username"]})
        if user and verify_password(request.form["password"],user['password']):
            if login_user(User(request.form["username"]), remember=True):
                return redirect(request.args.get("next") or url_for("index"))
        else:
            response = Response(status =401)
            return response
    return render_template('login.html',form=form)


@the_csrf.exempt
@app.route('/api/logout',methods=['GET','POST'])
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("index"))


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html', form = BasicForm())


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return render_template('404.html'), 404

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', type=int)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    date = request.args.get('seconds', type=str)
    time = request.args.get('time',type=str)
    values = datetime.strptime(date, "%Y-%m-%d").timestamp()
    date = int(values) + int(time)
    date = datetime.utcfromtimestamp(date).isoformat()
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, distance, date)
    close_time = acp_times.close_time(km, distance, date)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route('/new', methods=['POST'])
def new():
    """
    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.tododb.insert_one(item_doc)`
    """
    print("Submit should come overe here")
    log.info("I need it to come here")
    db.calcdb.delete_many({})
    data = request.form #getting a bunch of stuff here information
    close = request.form.getlist("closetimes[]", type=str)
    openlist = request.form.getlist("opentimes[]", type=str)
    km = request.form.getlist("kilo[]", type=str)
    miles = request.form.getlist("miles[]", type=str)
    loop = True
    index = 0
    while loop:
        if km[index] == "": # if nothing in the database then break
            break
        #writing for the display page and filling in paramaters as we go along the database
        items= {
            'km': "kilometers: " +str(km[index]),
            'mile': "miles: " +str(miles[index]),
            'open': "Opening Time: " +str(openlist[index]),
            'close': "Closing Time: " +str(close[index]) #adding stuff to database making move
        }
        log.info(items)
        db.calcdb.insert_one(items) #inserting database into db
        index += 1 #in the case that user enters more stuff into loop
    response = Response(status = 200)
    return response #submit is succesful !!!! :)

@app.route("/display") #Display the Database!!!
def todo():
    _items = db.calcdb.find()
    log.info(_items) #basically the same as the given todo function
    items = [item for item in _items]
    log.info(items)
    db.calcdb.delete_many({})
    return render_template('todo.html', items = items)


@app.route("/listAll/json")
@app.route("/listAll")
def get():
    _items = db.calcdb.find()
    items = [item for item in _items]
    opens = []
    for item in items:
        opens.append(item['open'])
    _items = db.calcdb.find()
    items = [item for item in _items]
    closes = []
    for item in items:
        closes.append(item['close'])
    return {'Opens': opens, 'Closes': closes}


@app.route("/listOpenOnly/json")
@app.route("/listOpenOnly")
def getlist(): #gonna repeat this method many times
    top = request.args.get('top')
    if top != None:
        try: #try to find items
            top = int(top)
            _items = db.calcdb.find()[0:top]
            items = [item for item in _items]
            opens = []
            for item in items: #finding open times
                opens.append(item['open'])

            return {'Opens': opens}
        except:
            return "wrong query"

    else:
        _items = db.calcdb.find()
        items = [item for item in _items]
        opens = []
        for item in items:
            opens.append(item['open'])

        return {'Opens': opens}

@app.route('/listCloseOnly/json')
@app.route('/listCloseOnly')
def getclose():
    top = request.args.get('top')
    if top != None:
        try:
            top = int(top)
            _items = db.calcdb.find()[0:top]
            items = [item for item in _items]
            closes = []
            for item in items:
                closes.append(item['close'])

            return {'Closes': closes}
        except:
            return "wrong query"

    else:
        _items = db.calcdb.find()
        items = [item for item in _items]
        closes = []
        for item in items:
            closes.append(item['closes'])

        return {'Closes': closes}

@app.route('/listAll/csv')
def getcsv():
    _items = db.calcdb.find()
    items = [item for item in _items]
    opens = [] #getting all open times and close times
    closes = []
    for item in items:
        opens.append(item['open'])
    for item in items:
        closes.append(item['close'])
    csv = 'Open TIme,'
    csv += 'Close time\n'
    stop = len(closes) - 1
# adding cases in case if its csv
    for i in range(len(closes)):
        if i != stop:
            csv += (opens[i] + ',' + closes[i] + '\n')
        else:
            csv += (opens[i] + ',' + closes[i])

    return csv

@app.route('/listOpenOnly/csv')
def get_opens():
    top = request.args.get('top')
    if top != None:
        try:
            top = int(top)
            _items = db.calcdb.find()[0:top]
            items = [item for item in _items]
            opens = []
            for item in items:
                opens.append(item['open'])
            csv = "\n".join(opens)
            csv = "Open time\n" + csv
            return csv
        except:
            return "wrong query"

    else:
        _items = db.calcdb.find()
        items = [item for item in _items]
        opens = []
        for item in items:
            opens.append(item['open'])
        csv = "\n".join(opens)
        csv = 'Open Time\n' + csv
        return csv

@app.route("/listCloseOnly/csv")
def get_close():
    top = request.args.get('top')
    if top != None:
        try:
            top = int(top)
            _items = db.calcdb.find()[0:top]
            items = [item for item in _items]
            closes = []
            for item in items:
                closes.append(item['open'])
            csv = "\n".join(closes)
            csv = "Close time\n" + csv
            return csv
        except:
            return "wrong query"
    else:
        _items = db.calcdb.find()
        items = [item for item in _items]
        closes = []
        for item in items:
            closes.append(item['open'])

        csv = "\n".join(closes)
        csv = 'Close Time\n' + csv

        return csv



class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }

# Create routes
# Another way, without decorators
api.add_resource(Laptop, '/laptop')

#############
app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
